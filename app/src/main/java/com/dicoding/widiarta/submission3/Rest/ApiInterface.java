package com.dicoding.widiarta.submission3.Rest;
import com.dicoding.widiarta.submission3.Model.MovieResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    static String myAPI = "6dc5f7499c44a739420e29abf7944de0";

    @GET("movie/top_rated")
    Call<MovieResponse> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("movie/{id}")
    Call<MovieResponse> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);

    @GET("movie/latest")
    Call<MovieResponse> getLatestMovie(@Query("api_key") String apiKey);

    @GET("movie/now_playing")
    Call<MovieResponse> getNowPlayingMovie(@Query("api_key") String apiKey);

    @GET("movie/popular")
    Call<MovieResponse> getPopularMovie(@Query("api_key") String apiKey);

    @GET("tv/latest")
    Call<MovieResponse> getLatestTv(@Query("api_key") String apiKey);

    @GET("tv/now_playing")
    Call<MovieResponse> getNowPlayingTv(@Query("api_key") String apiKey);

    @GET("tv/popular")
    Call<MovieResponse> getPopularTv(@Query("api_key") String apiKey);
}
